import argparse
from os.path import expanduser
from datetime import datetime
from pathlib import Path
import re


def create_dir(sub_dir_arg):
    home_dir = expanduser("~")
    if not sub_dir_arg:
        return f"{home_dir}/Desktop"

    # If the directory is an absolute path and starts with "/"
    if sub_dir_arg.startswith("/"):
        return sub_dir_arg

    return f"{home_dir}/{sub_dir_arg}".replace("//", "/")


def main():
    folders_in_base = [
        "_renders",
        "color_refs",
        "from_client",
        "offline",
        "postings",
        "source",
        "stills",
        "xml",
    ]

    folders_in_date = [
        "color_refs",
        "offline",
        "xml",
    ]

    argParser = argparse.ArgumentParser()
    argParser.add_argument("-s", "--sub-dir", required=False)
    argParser.add_argument("-c", "--client", required=True)

    args = argParser.parse_args()

    client_name = re.sub(r"(\s|-)", "_", args.client).lower()
    dir_to_create_folders = create_dir(args.sub_dir)

    now = datetime.now()
    date_string = f"{now.year}_{now.month:02d}_{now.day:02d}"
    date_string_with_hour = f"{now.year}_{now.month:02d}_{now.day:02d}/{now.hour:02d}"
    base_folder = f"{dir_to_create_folders}/{date_string}_{client_name}"
    for dir in folders_in_base:
        Path(f"{base_folder}/{dir}").mkdir(parents=True, exist_ok=True)

    source_path = f"{base_folder}/source/conform/{date_string_with_hour}"
    Path(source_path).mkdir(parents=True, exist_ok=True)

    for date_dir in folders_in_date:
        Path(f"{base_folder}/{date_dir}/{date_string_with_hour}").mkdir(
            parents=True, exist_ok=True
        )


main()
