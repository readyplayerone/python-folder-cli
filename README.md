# python-folder-cli

## Summary

This is a basic tool to create subdirectories with the following path,

```
<config dir>/Year/Month/Day/Hour
```

### Arguments

| Arg           | Long      | Short | Default |
| ------------- | --------- | ----- | ------- |
| sub directory | --sub-dir | -s    | Desktop |

If the sub-dir argument starts with a `/`, then it will be treated as an absolute path.

## Examples

### Basic

Creates a folder in `~/Desktop/2024/06/09/19`

```
python create.py
```

### With --sub-dir Argument

Creates a folder in `~/Desktop/dev/2024/06/09/19`

```
python create.py -s Desktop/dev
```

### With Absolute path in --sub-dir Argument

Creates a folder in `/Volumes/Backup/2024/06/09/19` in an external drive (Mac)

```
python create.py -s /Volumes/Backup
```
